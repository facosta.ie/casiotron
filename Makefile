PROJECT=Casiotron
SRC_OBJ=main.o ints.o clock.o globals.o serial.o lcd.o buffer.o queue.o msps.o startup_stm32f072xb.o system_stm32f0xx.o stm32f0xx_hal.o stm32f0xx_hal_gpio.o 
SRC_OBJ+=stm32f0xx_hal_rcc.o stm32f0xx_hal_cortex.o stm32f0xx_hal_uart.o stm32f0xx_hal_dma.o stm32f0xx_hal_irda.o
SRC_OBJ+=stm32f0xx_hal_usart.o stm32f0xx_hal_rtc.o stm32f0xx_hal_i2c.o stm32f0xx_hal_pwr.o stm32f0xx_hal_uart_ex.o stm32f0xx_hal_wwdg.o
SRC_OBJ+=stm32f0xx_hal_spi.o stm32f0xx_ll_rcc.o
VPATH=../../half0/Src
VPATH+=../../cmsisf0/startups
VPATH+=tasks
HPATH=-I../../half0/Inc
HPATH+=-I../../cmsisf0/core
HPATH+=-I../../cmsisf0/registers
HPATH+=-I tasks
HPATH+=-I .
CC=arm-none-eabi-gcc
MACH=cortex-m0
CFLAGS= -g3 -c -Wall -mcpu=$(MACH) -mthumb -mfloat-abi=soft -std=gnu11 -O0 $(HPATH) -DSTM32F072xB -DUSE_HAL_DRIVER
LDFLAGS_SH= -specs=rdimon.specs --specs=nano.specs -mcpu=$(MACH) -mthumb -mfloat-abi=soft -T STM32F072RBTx_FLASH.ld

all: $(PROJECT)

$(PROJECT):$(addprefix output/,$(PROJECT).elf)
	arm-none-eabi-objcopy -Oihex $< output/$(PROJECT).hex
	arm-none-eabi-objdump -S $< > output/$(PROJECT).lst
	arm-none-eabi-size --format=berkeley $<

output/$(PROJECT).elf:$(addprefix output/obj/,$(SRC_OBJ))
	$(CC) $(LDFLAGS_SH) -o $@ $^

output/obj/%.o:%.c
	$(CC) $(CFLAGS) -o $@ $^

output/obj/%.o:%.s
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm -rf output/obj/*.o output/*.elf output/*.hex output/*.lst

debug:
	arm-none-eabi-gdb output/$(PROJECT).elf -iex "set auto-load safe-path /"

open:
	openocd -f board/st_nucleo_f0.cfg