/*Globals.c*/
#include "globals.h"

void clear_RTC(_msg_serial* RTC_data)
{
    RTC_data->msg = 0u;
    RTC_data->param1 = 0u;
    RTC_data->param2 = 0u;
    RTC_data->param3 = 0u;
}