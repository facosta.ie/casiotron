/*Globals.h*/
#ifndef GLOBALS_H__
#define GLOBALS_H__
#include "stm32f0xx.h"
#include <stdio.h>
#include <string.h>

#define PIN_CS  GPIO_PIN_8
#define PIN_RS  GPIO_PIN_9
#define PIN_RST GPIO_PIN_7

#define NONE  0
#define TIME  1
#define DATE  2
#define ALARM 3

#define TRUE  1u
#define FALSE 0u

typedef struct
{
	uint8_t msg;	 // tipo de mensaje
	uint8_t param1;	 // hora o dia
    uint8_t param2;	 // minutos o mes
    uint8_t param3; // segundos o año
}_msg_serial;


typedef struct
{
    SPI_HandleTypeDef  	*SpiHandler;
    GPIO_TypeDef	   	*RstPort;
    uint32_t			RstPin;
    GPIO_TypeDef		*RsPort;
    uint32_t			RsPin;
    GPIO_TypeDef		*CsPort;
    uint32_t			CsPin;
//agregar más elementos si se requieren
}LCD_HandleTypeDef;

typedef struct
{
    uint8_t  	*Buffer;
    uint32_t	Elements;
    uint32_t	Head;
    uint32_t	Tail;
    uint8_t	    Empty;
    uint8_t	    Full;
}BUFFER_HandleTypeDef;

typedef enum 
{
	CLEAR_CMD = 0u,
	NEW_CMD,
}Usart_states;

typedef struct
{
    void  	    *Buffer;
    uint32_t	Elements;
    uint8_t     Size;     //tamaño del tipo de elementos
    uint32_t	Head;
    uint32_t	Tail;
    uint8_t	    Empty;
    uint8_t	    Full;
    //agregar más elementos si se requieren
}QUEUE_HandleTypeDef;

typedef struct
{
I2C_HandleTypeDef  	*I2cHandler;
GPIO_TypeDef		*AlertPort;
uint32_t			 AlertPin;
//agregar más elementos si se requieren
}TEMP_HandleTypeDef;


RTC_HandleTypeDef RtcHandle;
UART_HandleTypeDef Handler_Uart;
SPI_HandleTypeDef SpiHandle;
LCD_HandleTypeDef LcdHandle;
BUFFER_HandleTypeDef BufferCirc;
_msg_serial  RTC_Data_Config;

extern uint32_t myTimer;
extern uint32_t tickTimer;
extern uint32_t tmrHeartBeat;
extern uint8_t  RTC_config_flag;
extern uint8_t  Flag_AlarmSet;

uint8_t RxBuff;
uint8_t BuffRx[100u]; 

void clear_RTC(_msg_serial* RTC_data);
_msg_serial MsgBuffer[10];
_msg_serial MsgDataConfig;

QUEUE_HandleTypeDef SerialQueue;
QUEUE_HandleTypeDef MsgQueue;

I2C_HandleTypeDef I2cHandle;
uint8_t I2CBuffer[2];


#endif