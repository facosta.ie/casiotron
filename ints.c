#include "globals.h"

void NMI_Handler(void)
{

}

void HardFault_Handler(void)
{
    /* Go to infinite loop when Hard Fault exception occurs */
    while (1)
    {
    }
}

void SVC_Handler(void)
{

}

void PendSV_Handler(void)
{

}

void RTC_IRQHandler(void)
{
  HAL_RTC_AlarmIRQHandler(&RtcHandle);
}

void USART2_IRQHandler( void )
{
    HAL_UART_IRQHandler(&Handler_Uart);
}

void EXTI4_15_IRQHandler(void)
{
	if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_13))
	{
        HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
	}
    if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_8))
	{
        HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_8);
	}
} 

void SysTick_Handler(void)
{
    HAL_IncTick();
}