#include "globals.h"
#include "clock.h"
#include "serial.h"
#include "lcd.h"
#include "buffer.h"
#include "queue.h"

void HeartBeat_Init(void);
void SystemClock_Config(void);
void HeartBeat(void);
extern void initialise_monitor_handles(void);
void WWDG_Config(void);
void KickDog(void);
void Queue_init(void);

uint32_t myTimer = 0u;
uint32_t tickTimer = 0u;
uint32_t heart_beat_tick = 0u;
uint32_t WWDT_tick = 0u;
uint32_t Serial_tick = 0u;

WWDG_HandleTypeDef  WWDG_Handle;

int main(void)
{
 	initialise_monitor_handles();
  SystemClock_Config();
	HAL_Init();
  Serial_Init();
	RTC_Init();
  HeartBeat_Init();
  MOD_LCD_Init(&LcdHandle);
  printf("CASIOTRON INIT\n\r");
  Queue_init();
  I2C_init();
  Serial_tick = HAL_GetTick();
  WWDG_Config();

  while(1)
  {
    Clock_task();
    if((HAL_GetTick() - Serial_tick) > 100u)
    {
      Serial_Task();
      Serial_tick = HAL_GetTick();
    }
    HeartBeat();
    KickDog();
  }
}

/**---------------------------------------------------------------
Brief.-  This function sets the pins needed for using the heartbeat
Param.-  void
Return.- void
---------------------------------------------------------------*/
void HeartBeat_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pin = GPIO_PIN_5;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

/**---------------------------------------------------------------
Brief.-  This function toggles a led every 300ms
Param.-  void
Return.- void
---------------------------------------------------------------*/
void HeartBeat(void)
{
  if((HAL_GetTick( ) - heart_beat_tick) > 300u)
  {
    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
    heart_beat_tick = HAL_GetTick();
  }
}

/**---------------------------------------------------------------
Brief.-  This function configures the WWDG used for the app
Param.-  void
Return.- void
---------------------------------------------------------------*/
void WWDG_Config(void)
{
    __HAL_RCC_WWDG_CLK_ENABLE();
    WWDG_Handle.Instance = WWDG;
    WWDG_Handle.Init.Prescaler = WWDG_PRESCALER_4;
    WWDG_Handle.Init.Window    = 0x7F;
    WWDG_Handle.Init.Counter   = 0x7F;
    WWDG_Handle.Init.EWIMode   = WWDG_EWI_DISABLE;
    (void)HAL_WWDG_Init(&WWDG_Handle);
}

/**---------------------------------------------------------------
Brief.-  This function refresh the WWDG to avoid the reset
Param.-  void
Return.- void
---------------------------------------------------------------*/
void KickDog(void)
{
    if((HAL_GetTick() - WWDT_tick) > 60u)
  {
  	HAL_WWDG_Refresh(&WWDG_Handle);
    WWDT_tick = HAL_GetTick();
  }
}

void Queue_init(void)
{
  SerialQueue.Buffer = (void*) BuffRx;
  SerialQueue.Elements = 100u;
  SerialQueue.Size = sizeof(RxBuff);
  HIL_QUEUE_Init( &SerialQueue );

  MsgQueue.Buffer = (void*) MsgBuffer;
  MsgQueue.Elements = 10u;
  MsgQueue.Size = sizeof(_msg_serial);
  HIL_QUEUE_Init(&MsgQueue);
}

void I2C_init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  __HAL_RCC_I2C1_CLK_ENABLE();
  /* configuramos pines B6 y B7 en funcion alterna I2C1 */
  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD; /*pines con colector abierto*/
  GPIO_InitStruct.Pull = GPIO_PULLUP; /*weak pullups*/
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF1_I2C1; /*modo I2C*/
  GPIO_InitStruct.Pin = GPIO_PIN_6 | GPIO_PIN_7;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  I2cHandle.Instance = I2C1;
  I2cHandle.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  I2cHandle.Init.Timing = 0x00100107;
  I2cHandle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  I2cHandle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  I2cHandle.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  /*aplicamos configuracion al modulo I2C1*/
  HAL_I2C_Init(&I2cHandle);
}