/*MSPS.c*/
#include "globals.h"


/**---------------------------------------------------------------
Brief.-  This function enables the GPIO ports used for the application
Param.-  void
Return.- void
---------------------------------------------------------------*/
void HAL_MspInit(void)
{
    __HAL_RCC_GPIOA_CLK_ENABLE(); 
    __HAL_RCC_GPIOB_CLK_ENABLE(); 
    __HAL_RCC_GPIOC_CLK_ENABLE(); 
}

/**---------------------------------------------------------------
Brief.-  This function configures the system clock speed and for the
         peripherals
Param.-  void
Return.- void
---------------------------------------------------------------*/
void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;
    /* Select HSI48 Oscillator as PLL source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48;
    RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI48;
    RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV2;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL2;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);
    /* Select PLL as system clock source and configure the HCLK and PCLK1 clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV8;
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1);

}

/**---------------------------------------------------------------
Brief.-  This function sets the pins needed for using the uart
Param.-  void
Return.- void
---------------------------------------------------------------*/
void HAL_UART_MspInit( UART_HandleTypeDef *huart )
{
    GPIO_InitTypeDef GPIO_InitStruct; 
    __HAL_RCC_USART2_CLK_ENABLE();

    GPIO_InitStruct.Pin = GPIO_PIN_2 | GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF1_USART2;
 
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

/**---------------------------------------------------------------
Brief.-  This function sets the pins needed for using the SPI
Param.-  void
Return.- void
---------------------------------------------------------------*/
void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
    GPIO_InitTypeDef  GPIO_InitStruct;
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
    /* Enable SPI clock */
     __HAL_RCC_SPI1_CLK_ENABLE();

    GPIO_InitStruct.Pin = PIN_RST; /*pines a configurar*/
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP; /*salida tipo push-pull*/
    GPIO_InitStruct.Pull = GPIO_NOPULL; /*pin sin pull-up ni*/
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = PIN_CS|PIN_RS; /*pines a configurar*/
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP; /*salida tipo push-pull*/
    GPIO_InitStruct.Pull = GPIO_NOPULL; /*pin sin pull-up ni*/
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        /* SPI SCK GPIO pin configuration  */
    GPIO_InitStruct.Pin = GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF0_SPI1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

/**---------------------------------------------------------------
Brief.-  This function initializes the SPI needed for the LCD 
Param.-  void
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_MspInit( LCD_HandleTypeDef *hlcd )
{
    SPI_Init();
}


/*
void HAL_RTC_MspInit(RTC_HandleTypeDef *hrtc);
{

}*/


