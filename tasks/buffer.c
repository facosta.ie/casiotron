/*buffer.c*/
#include "buffer.h"

/**---------------------------------------------------------------
Brief.-  This function initializes the buffer to start the reception
         of data, sets tail, head, full and empty flags. 
Param.-  hbuffer this structure contains the data to be initialized
Return.- void
---------------------------------------------------------------*/
void HIL_BUFFER_Init( BUFFER_HandleTypeDef *hbuffer )
{
    hbuffer->Head  = 0u;
    hbuffer->Tail  = 0u;
    hbuffer->Empty = 1u;
    hbuffer->Full  = 0u;
}
/**---------------------------------------------------------------
Brief.-  This function writes a new byte in the buffer if there's 
         space available
Param.-  hbuffer - this function contains the data types to be copied and
         other variables for the operation of the buffer
         data - This pointer cotains the data to be written in the buffer
Return.- uint8 returns 1 if it could be written and 0 if not.
---------------------------------------------------------------*/
void HIL_BUFFER_Write( BUFFER_HandleTypeDef *hbuffer, uint8_t data )
{   
    if(hbuffer->Full != TRUE)
    {
        hbuffer->Buffer[hbuffer->Head] = data;
        hbuffer->Head++;
        hbuffer->Head %= hbuffer->Elements;

        if(hbuffer->Head == hbuffer->Tail)
        {
            hbuffer->Full = FALSE;
        }
        hbuffer->Empty = FALSE;
    }
}
/**---------------------------------------------------------------
Brief.-  This function takes one byte from the buffer and is returned
         in the function if there are data available to be read
Param.-  hbuffer - this function contains the data types to be copied and
         other variables for the operation of the queue 
Return.- uint8 returns 1 byte of information
---------------------------------------------------------------*/
uint8_t HIL_BUFFER_Read( BUFFER_HandleTypeDef *hbuffer )
{
    uint8_t data;

    if(hbuffer->Empty != TRUE)
    { 
        data = hbuffer->Buffer[hbuffer->Tail];
        hbuffer->Tail++;
        hbuffer->Tail %= hbuffer->Elements;

        if(hbuffer->Tail == hbuffer->Head)
        {
            hbuffer->Empty = TRUE;
        }
        hbuffer->Full = FALSE;
    }
 
    return data;
}
/**---------------------------------------------------------------
Brief.-  This function verifies if the queue contains elements to be
         read
Param.-  hbuffer - this function contains the data types to be copied and
         other variables for the operation of the queue 
Return.- uint8 returns 1 if there are no more  elements to be read 
         and 0 if there are at least one element to be read
---------------------------------------------------------------*/
uint8_t HIL_BUFFER_IsEmpty( BUFFER_HandleTypeDef *hbuffer )
{
    return hbuffer->Empty;
}