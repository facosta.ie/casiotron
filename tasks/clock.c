/*clock.c*/
#include "globals.h"
#include "clock.h"

uint8_t Info_state  = PRINT_TIME;
uint8_t lastSecond  = 0u;
uint8_t minuteCntr  = 0u;
uint8_t flag_alarm  = 0u;
uint8_t flag_button = 0u;
uint8_t flag_show   = 0u;
uint8_t RTC_config_flag = 0;
uint8_t aTxBuffer[5] = {0x55,0x88,0xAA,0xBB,0xCC};
uint8_t Flag_AlarmSet = 0u;
uint8_t Toggle_alarm = 0u;
uint8_t flag_Temp_alarm = 0u;
uint8_t TempReg = 0x05u;
const char Date_Print_Day[7][3] = {
  "Lu",
  "Ma",
  "Mi",
  "Ju",
  "Vi",
  "Sa",
  "Do"
};

const char Date_Print_Month[13][5] = {
  "STA,", //created only to align index with RTC results
  "ENE,",
  "FEB,",
  "MAR,",
  "ABR,",
  "MAY,",
  "JUN,",
  "JUL,",
  "AGO,",
  "SEP,",
  "OCT,",
  "NOV,",
  "DIC,"
};

/**---------------------------------------------------------------
Brief.-  This function configures and initializes the RTC pins and
         the interrupts for RTC and clock
Param.-  void
Return.- void
---------------------------------------------------------------*/
void RTC_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  __HAL_RCC_PWR_CLK_ENABLE();
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSI_ENABLE();
  while(__HAL_RCC_GET_FLAG(RCC_FLAG_LSIRDY) == 0);
  __HAL_RCC_RTC_ENABLE();
  __HAL_RCC_RTC_CONFIG(RCC_RTCCLKSOURCE_LSI);

  RtcHandle.Instance = RTC;
  RtcHandle.Init.HourFormat     = RTC_HOURFORMAT_24;
  RtcHandle.Init.AsynchPrediv   = RTC_ASYNCH_PREDIV;
  RtcHandle.Init.SynchPrediv    = RTC_SYNCH_PREDIV;
  RtcHandle.Init.OutPut         = RTC_OUTPUT_DISABLE;
  RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  RtcHandle.Init.OutPutType     = RTC_OUTPUT_TYPE_OPENDRAIN;
  HAL_RTC_Init(&RtcHandle);

  HAL_NVIC_SetPriority(RTC_IRQn, 2u, 2u);
	__HAL_RTC_ALARMA_ENABLE(&RtcHandle);
	HAL_NVIC_EnableIRQ(RTC_IRQn);

	GPIO_InitStruct.Mode  = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  GPIO_InitStruct.Pin   = GPIO_PIN_13;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  HAL_NVIC_SetPriority(EXTI4_15_IRQn,1u,1u);
	HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
}
/**---------------------------------------------------------------
Brief.-  This function configures the RTC with the time desired
Param.-  Data_Time - Structure which contains the information
         for the time to configure
Return.- void
---------------------------------------------------------------*/
void RTC_Set_Time(_msg_serial* Data_Time)
{
  uint8_t retval = 0;
	RTC_TimeTypeDef stimestructure;

  stimestructure.Hours =   Data_Time->param1;
  stimestructure.Minutes = Data_Time->param2;
  stimestructure.Seconds = Data_Time->param3;
  stimestructure.TimeFormat = RTC_HOURFORMAT12_AM;
  stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE ;
  stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;

  retval = HAL_RTC_SetTime(&RtcHandle,&stimestructure,RTC_FORMAT_BCD);
  
  if(retval != HAL_OK)
  {
  }
  else
  {
    RTC_config_flag = 0;
    clear_RTC(Data_Time);
  }
}
/**---------------------------------------------------------------
Brief.-  This function configures the RTC with the Date desired
Param.-  Data_Date - Structure which contains the information
         for the Date to configure
Return.- void
---------------------------------------------------------------*/
void RTC_Set_Date(_msg_serial* Data_Date)
{
  uint8_t retval = 0;
  RTC_DateTypeDef sdatestructure;
  
  sdatestructure.Date  = Data_Date->param1;
  sdatestructure.Month = Data_Date->param2;
  sdatestructure.Year  = Data_Date->param3;    
  sdatestructure.WeekDay = RTC_WEEKDAY_TUESDAY;
  
  retval = HAL_RTC_SetDate(&RtcHandle,&sdatestructure,RTC_FORMAT_BCD);
  if (retval != HAL_OK)
  {
  }
  else
  {
    RTC_config_flag = 0;
    clear_RTC(Data_Date);
  }
}
/**---------------------------------------------------------------
Brief.-  This function configures the RTC with the alarm desired
Param.-  Data_Alarm - Structure which contains the information
         for the alarm to configure
Return.- void
---------------------------------------------------------------*/
void RTC_Set_Alarm(_msg_serial* Data_Alarm)
{
	RTC_AlarmTypeDef salarmstructure;
  uint8_t retval = 0;
  salarmstructure.Alarm                = RTC_ALARM_A;
  salarmstructure.AlarmDateWeekDay     = RTC_WEEKDAY_MONDAY;
  salarmstructure.AlarmDateWeekDaySel  = RTC_ALARMDATEWEEKDAYSEL_DATE;
  salarmstructure.AlarmMask            = RTC_ALARMMASK_DATEWEEKDAY;
  salarmstructure.AlarmSubSecondMask   = RTC_ALARMSUBSECONDMASK_NONE;
  salarmstructure.AlarmTime.TimeFormat = RTC_HOURFORMAT_24;
  salarmstructure.AlarmTime.Hours      = Data_Alarm->param1;
  salarmstructure.AlarmTime.Minutes    = Data_Alarm->param2;
  salarmstructure.AlarmTime.Seconds    = Data_Alarm->param3;
  salarmstructure.AlarmTime.SubSeconds = 0xF9;

  retval = HAL_RTC_SetAlarm_IT(&RtcHandle,&salarmstructure,RTC_FORMAT_BCD);
  
  if(retval != HAL_OK)
  {
  }
  else  
  {
    HAL_NVIC_SetPriority(RTC_IRQn, 2u, 2u);
	  __HAL_RTC_ALARMA_ENABLE(&RtcHandle);
  	HAL_NVIC_EnableIRQ(RTC_IRQn);
    RTC_config_flag = 0;
    clear_RTC(Data_Alarm);
  }
}
/**---------------------------------------------------------------
Brief.-  This function is triggered when the alarm event is active
Param.-  hrtc - Is used to reset event flag
Return.- void
---------------------------------------------------------------*/
void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
    flag_alarm = 1u;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13) == 0u)
  {
      flag_button = 1u;
      flag_show = 1u;
  }
  if((HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_8) == 0u))
  {
    flag_Temp_alarm = 1u;
  }
}
/**---------------------------------------------------------------
Brief.-  This function gets the date and time from RTC verify all
         the flags related to the clock and takes the decision to
         print in the LCD the current status of date, time or alarm
Param.-  void
Return.- void
---------------------------------------------------------------*/
 void Clock_task(void)
{
  RTC_DateTypeDef datestructureget;
  RTC_TimeTypeDef timestructureget;
  RTC_AlarmTypeDef alarmstructureget;

  HAL_RTC_GetTime(&RtcHandle, &timestructureget, RTC_FORMAT_BIN);
  HAL_RTC_GetDate(&RtcHandle, &datestructureget, RTC_FORMAT_BIN);

  switch (Info_state) 
  {
    case PRINT_IDLE:
    {
      if((flag_button == 0u) && (flag_alarm == 0u))
      {
         Info_state = PRINT_TIME;
      }
      else if ((flag_button == 0u) && (flag_alarm == 1u))
      {
        Info_state = PRINT_ALARM_ON;
      }
      else if((flag_button == 1u) && (flag_alarm == 1u))
      {
        minuteCntr = 0xFFu;
        Toggle_alarm = 0u;
        Info_state = PRINT_ALARM_ON;
      }
      else if(((flag_button == 1u) && (flag_alarm == 0u)) )
      {
          Info_state = PRINT_ALARM_INFO;
      }
      break;
    }
    case PRINT_TIME:
    {
      if(timestructureget.Seconds != lastSecond)
      {
          LCD_Print_Time(&timestructureget);
          lastSecond = timestructureget.Seconds;
          Info_state = PRINT_DATE;
          HAL_I2C_Master_Transmit(&I2cHandle, 0x3F, &TempReg,1, 10000);
          HAL_I2C_Master_Receive(&I2cHandle, 0x3E, I2CBuffer, 2, 10000);
          //printf("T=%X",I2CBuffer[0]);
          //printf("%X\n\r",I2CBuffer[1]);
      }
      break;
    }
    case PRINT_DATE:
    {
      LCD_Print_Date(&datestructureget);
      Info_state = PRINT_IDLE;
    }
    case PRINT_ALARM_ON:
    {
      if((timestructureget.Seconds != lastSecond) && (minuteCntr < 60u))
      {
        if(Toggle_alarm == 1u)
        {
          LCD_Print_Alarm();
          Toggle_alarm = 0u;
        }
        else
        {
          LCD_Print_Alarm_Clr();
          Toggle_alarm = 1u;
        }
        
        lastSecond = timestructureget.Seconds;
        minuteCntr++;
      }
      if(minuteCntr > 59)
      {
        minuteCntr  = 0u;
        flag_alarm  = 0u;
        flag_button = 0u;
        Flag_AlarmSet = 0u;
        Toggle_alarm = 0u;
        LCD_Print_Alarm_Clr();
      }
      Info_state = PRINT_IDLE;
      break;
    }
    case PRINT_ALARM_INFO:
    {
      HAL_RTC_GetAlarm(&RtcHandle, &alarmstructureget,RTC_ALARM_A, RTC_FORMAT_BIN);
      
      if(flag_button == 1u && flag_show == 1u)
      {
        
        LCD_Print_Alarm_Config(&alarmstructureget);

        flag_show = 0u;
      }
      if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_13) == 0u)
      {
      }
      else
      {
        flag_button = 0u;
      }
      Info_state = PRINT_IDLE;
      break;
    }
      default:
      break;
  }
  
  RTC_Set_Config();
}
/**---------------------------------------------------------------
Brief.-  This function reads the configuration to be set and  
         takes the decision to configure it, date, time or alarm.
Param.-  void
Return.- void
---------------------------------------------------------------*/
void RTC_Set_Config(void)
{
   //if(RTC_config_flag == 1)
   if(HIL_QUEUE_IsEmpty() == FALSE)
  {
    HIL_QUEUE_Read(&MsgQueue,&MsgDataConfig);

    switch (MsgDataConfig.msg)
    {
      case TIME:
      {
        RTC_Set_Time(&MsgDataConfig);
      }
      break;
      case DATE:
      {
        RTC_Set_Date(&MsgDataConfig);
      }
      break;
      case ALARM:
      {
        RTC_Set_Alarm(&MsgDataConfig);
      }
      break;
    
      default:
      break;
    }
  }
}

/**---------------------------------------------------------------
Brief.-  This function prints the date assigned in the parameter
Param.-  StructureDate - Contains the date to be printed
Return.- void
---------------------------------------------------------------*/
void LCD_Print_Date(RTC_DateTypeDef *StructureDate)
{
  char auxDate[3] = {0u,0u,0u};
  char auxYear[5] = {0u,0u,0u,0u,0u};
  uint16_t W_Day  = 0u;

  W_Day = wd((uint16_t)(StructureDate->Year + 2000u),(uint16_t)StructureDate->Month,(uint16_t)StructureDate->Date);
  
  itoa(StructureDate->Date,auxDate,10u);
  itoa((StructureDate->Year + 2000u),auxYear,10u);

  MOD_LCD_SetCursor(&LcdHandle,1u, 0u);
  MOD_LCD_String(&LcdHandle,Date_Print_Month[StructureDate->Month],sizeof(Date_Print_Month[0] - 1));
  MOD_LCD_String(&LcdHandle,auxDate, 3u);
  MOD_LCD_SetCursor(&LcdHandle,8u, 0u);
  MOD_LCD_String(&LcdHandle,auxYear,5u);
  MOD_LCD_SetCursor(&LcdHandle,13u, 0u);
  MOD_LCD_String(&LcdHandle,Date_Print_Day[W_Day],sizeof(Date_Print_Day[0] - 1));
}

/**---------------------------------------------------------------
Brief.-  This function prints the Time assigned in the parameter
Param.-  StructureTime - Contains the time to be printed
Return.- void
---------------------------------------------------------------*/
void LCD_Print_Time(RTC_TimeTypeDef *StructureTime)
{
   char auxHours[3]   = {0u,0u,0u};
  char auxMinutes[3] = {0u,0u,0u};
  char auxSeconds[3] = {0u,0u,0u};

  itoa(StructureTime->Hours,auxHours,10u);
  itoa(StructureTime->Minutes,auxMinutes,10u);
  itoa(StructureTime->Seconds,auxSeconds,10u);

  MOD_LCD_SetCursor(&LcdHandle, 0, 1);
  MOD_LCD_Data(&LcdHandle,' ');
  MOD_LCD_Data(&LcdHandle,' ');
  MOD_LCD_Data(&LcdHandle,' ');
  MOD_LCD_Data(&LcdHandle,' ');
  MOD_LCD_String(&LcdHandle,auxHours, 2u);
  MOD_LCD_Data(&LcdHandle,':');
  MOD_LCD_String(&LcdHandle,auxMinutes, 2u);
  MOD_LCD_Data(&LcdHandle,':');
  MOD_LCD_String(&LcdHandle,auxSeconds,2u);
  MOD_LCD_Data(&LcdHandle,' ');
  MOD_LCD_Data(&LcdHandle,' ');
  
  if(Flag_AlarmSet == 1u)
    {
      MOD_LCD_SetCursor(&LcdHandle, 14u, 1u);
      MOD_LCD_Data(&LcdHandle,'A');
      MOD_LCD_Data(&LcdHandle,' ');
    }
    else
    {
      MOD_LCD_SetCursor(&LcdHandle, 14u, 1u);
      MOD_LCD_Data(&LcdHandle,' ');
      MOD_LCD_Data(&LcdHandle,' ');
    }
}

/**---------------------------------------------------------------
Brief.-  This function prints the caracters "******" when alarm
         is activated
Param.-  void
Return.- void
---------------------------------------------------------------*/
void LCD_Print_Alarm(void)
{
  MOD_LCD_SetCursor(&LcdHandle, 0u, 1u);
  MOD_LCD_Data(&LcdHandle,'*');
  MOD_LCD_Data(&LcdHandle,'*');
  MOD_LCD_Data(&LcdHandle,'*');
  MOD_LCD_SetCursor(&LcdHandle, 13u, 1u);
  MOD_LCD_Data(&LcdHandle,'*');
  MOD_LCD_Data(&LcdHandle,'*');
  MOD_LCD_Data(&LcdHandle,'*');
}

/**---------------------------------------------------------------
Brief.-  This function clears the LCD display when an alarm is off 
Param.-  void
Return.- void
---------------------------------------------------------------*/
void LCD_Print_Alarm_Clr(void)
{
  MOD_LCD_SetCursor(&LcdHandle, 0u, 1u);
  MOD_LCD_Data(&LcdHandle,' ');
  MOD_LCD_Data(&LcdHandle,' ');
  MOD_LCD_Data(&LcdHandle,' ');
  MOD_LCD_SetCursor(&LcdHandle, 13u, 1u);
  MOD_LCD_Data(&LcdHandle,' ');
  MOD_LCD_Data(&LcdHandle,' ');
  MOD_LCD_Data(&LcdHandle,' ');
}
/**---------------------------------------------------------------
Brief.-  This function configures the alarm assigned in the structure
Param.-  StructureAlarm - Contains the Alarm to be configured
Return.- void
---------------------------------------------------------------*/
void LCD_Print_Alarm_Config(RTC_AlarmTypeDef *StructureAlarm)
{
  char auxHours[3]   = {0u,0u,0u};
  char auxMinutes[3] = {0u,0u,0u};
  char auxSeconds[3] = {0u,0u,0u};

  itoa(StructureAlarm->AlarmTime.Hours,auxHours,10u);
  itoa(StructureAlarm->AlarmTime.Minutes,auxMinutes,10u);
  itoa(StructureAlarm->AlarmTime.Seconds,auxSeconds,10u);

  if(Flag_AlarmSet == 1u)
  {
    MOD_LCD_SetCursor(&LcdHandle, 0u, 1u);
    MOD_LCD_Data(&LcdHandle,'A');
    MOD_LCD_Data(&LcdHandle,'L');
    MOD_LCD_Data(&LcdHandle,'A');
    MOD_LCD_Data(&LcdHandle,'R');
    MOD_LCD_Data(&LcdHandle,'M');
    MOD_LCD_Data(&LcdHandle,' ');
    MOD_LCD_SetCursor(&LcdHandle, 6u, 1u);
    MOD_LCD_String(&LcdHandle,auxHours, 2u);
    MOD_LCD_Data(&LcdHandle,':');
    MOD_LCD_String(&LcdHandle,auxMinutes, 2u);
    MOD_LCD_Data(&LcdHandle,':');
    MOD_LCD_String(&LcdHandle,auxSeconds,2u);
    MOD_LCD_Data(&LcdHandle,' ');
    MOD_LCD_Data(&LcdHandle,' ');
  }
  else
  {
    MOD_LCD_SetCursor(&LcdHandle, 0u, 1u);
    MOD_LCD_Data(&LcdHandle,'N');
    MOD_LCD_Data(&LcdHandle,'O');
    MOD_LCD_Data(&LcdHandle,' ');
    MOD_LCD_Data(&LcdHandle,'A');
    MOD_LCD_Data(&LcdHandle,'L');
    MOD_LCD_Data(&LcdHandle,'A');
    MOD_LCD_Data(&LcdHandle,'R');
    MOD_LCD_Data(&LcdHandle,'M');
    MOD_LCD_Data(&LcdHandle,' ');
    MOD_LCD_Data(&LcdHandle,'C');
    MOD_LCD_Data(&LcdHandle,'O');
    MOD_LCD_Data(&LcdHandle,'N');
    MOD_LCD_Data(&LcdHandle,'F');
    MOD_LCD_Data(&LcdHandle,'I');
    MOD_LCD_Data(&LcdHandle,'G');
    MOD_LCD_Data(&LcdHandle,' ');
  }

}
/**---------------------------------------------------------------
Brief.-  This function returns the week day for the date assigned
Param.-  year : year to get the week day
         month: month to get the week day
         day  : day to get the week day
Return.- uint16 returns the week day
---------------------------------------------------------------*/
uint16_t wd(uint16_t year, uint16_t month, uint16_t day) 
{
    uint16_t wday = 0u;
    wday = (day  + ((153u * (month + 12u * ((14u - month) / 12u) - 3u) + 2u) / 5u) \
        +  (365u * (year + 4800u - ((14u - month) / 12u)))              \
        +  ((year + 4800u - ((14u - month) / 12u)) / 4u)                \
        -  ((year + 4800u - ((14u - month) / 12u)) / 100u)              \
        +  ((year + 4800u - ((14u - month) / 12u)) / 400u)              \
        -  32045u                                                       \
      ) % 7u;
     return wday;   
}