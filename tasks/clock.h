/*Clock.h*/
#ifndef CLOCK_H__
#define CLOCK_H__

#define RTC_CLOCK_SOURCE_LSI

#ifdef RTC_CLOCK_SOURCE_LSI
#define RTC_ASYNCH_PREDIV    0x7F
#define RTC_SYNCH_PREDIV     0x00F9
#endif

#ifdef  RTC_CLOCK_SOURCE_LSE
#define RTC_ASYNCH_PREDIV  0x7F
#define RTC_SYNCH_PREDIV   0x00FF
#endif

enum STATE_PRINT
{
  PRINT_IDLE = 0u,
  PRINT_TIME,
  PRINT_DATE,
  PRINT_ALARM_ON,
  PRINT_ALARM_INFO,
};

/**---------------------------------------------------------------
Brief.-  This function configures and initializes the RTC pins and
         the interrupts for RTC and clock
Param.-  void
Return.- void
---------------------------------------------------------------*/
void RTC_Init(void);
/**---------------------------------------------------------------
Brief.-  This function configures the RTC with the time desired
Param.-  Data_Time - Structure which contains the information
         for the time to configure
Return.- void
---------------------------------------------------------------*/
void RTC_Set_Time(_msg_serial* Data_Time);
/**---------------------------------------------------------------
Brief.-  This function configures the RTC with the Date desired
Param.-  Data_Date - Structure which contains the information
         for the Date to configure
Return.- void
---------------------------------------------------------------*/
void RTC_Set_Date(_msg_serial* Data_Date);
/**---------------------------------------------------------------
Brief.-  This function configures the RTC with the alarm desired
Param.-  Data_Alarm - Structure which contains the information
         for the alarm to configure
Return.- void
---------------------------------------------------------------*/
void RTC_Set_Alarm(_msg_serial* Data_Alarm);
/**---------------------------------------------------------------
Brief.-  This function gets the date and time from RTC verify all
         the flags related to the clock and takes the decision to
         print in the LCD the current status of date, time or alarm
Param.-  void
Return.- void
---------------------------------------------------------------*/
void Clock_task(void);
/**---------------------------------------------------------------
Brief.-  This function reads the configuration to be set and  
         takes the decision to configure it, date, time or alarm.
Param.-  void
Return.- void
---------------------------------------------------------------*/
void RTC_Set_Config(void);

/**---------------------------------------------------------------
Brief.-  This function prints the caracters "******" when alarm
         is activated
Param.-  void
Return.- void
---------------------------------------------------------------*/
void LCD_Print_Alarm(void);
/**---------------------------------------------------------------
Brief.-  This function prints the Time assigned in the parameter
Param.-  StructureTime - Contains the time to be printed
Return.- void
---------------------------------------------------------------*/
void LCD_Print_Time(RTC_TimeTypeDef *StructureTime);
/**---------------------------------------------------------------
Brief.-  This function configures the alarm assigned in the structure
Param.-  StructureAlarm - Contains the Alarm to be configured
Return.- void
---------------------------------------------------------------*/
void LCD_Print_Alarm_Config(RTC_AlarmTypeDef *StructureAlarm);
/**---------------------------------------------------------------
Brief.-  This function prints the date assigned in the parameter
Param.-  StructureDate - Contains the date to be printed
Return.- void
---------------------------------------------------------------*/
void LCD_Print_Date(RTC_DateTypeDef *StructureDate);
/**---------------------------------------------------------------
Brief.-  This function clears the LCD display when an alarm is off 
Param.-  void
Return.- void
---------------------------------------------------------------*/
void LCD_Print_Alarm_Clr(void);

/**---------------------------------------------------------------
Brief.-  This function returns the week day for the date assigned
Param.-  year : year to get the week day
         month: month to get the week day
         day  : day to get the week day
Return.- uint16 returns the week day
---------------------------------------------------------------*/
uint16_t wd(uint16_t year, uint16_t month, uint16_t day); 


#endif
