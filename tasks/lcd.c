/*lcd.c*/
#include "globals.h"
#include "lcd.h"

void DWT_Init(void);
void DWT_Delay(uint32_t us);
uint32_t multiplier;
uint8_t RxData;

/**---------------------------------------------------------------
Brief.-  This function initializes the SPI for the LCD display
Param.-  void
Return.- void
---------------------------------------------------------------*/
void SPI_Init(void)
{
    SpiHandle.Instance               = SPI1;
    SpiHandle.Init.Mode              = SPI_MODE_MASTER;
    SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
    SpiHandle.Init.Direction         = SPI_DIRECTION_2LINES;
    SpiHandle.Init.CLKPhase          = SPI_PHASE_2EDGE;
    SpiHandle.Init.CLKPolarity       = SPI_POLARITY_HIGH;
    SpiHandle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
    SpiHandle.Init.CRCPolynomial     = 7;
    SpiHandle.Init.DataSize          = SPI_DATASIZE_8BIT;
    SpiHandle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
    SpiHandle.Init.NSS               = SPI_NSS_SOFT;
    SpiHandle.Init.TIMode            = SPI_TIMODE_DISABLE;

    HAL_GPIO_WritePin(GPIOA,GPIO_PIN_8,SET);
    HAL_SPI_Init(&SpiHandle);
}
/**---------------------------------------------------------------
Brief.-  This function initializes, the SPI nneded for the control
         of the LCD display
Param.-  hlcd - This structure contains the elements to configure
         the pins, and SPI handler for this LCD display
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_Init( LCD_HandleTypeDef *hlcd )
{
    MOD_LCD_MspInit(&hlcd);

    LcdHandle.SpiHandler = &SpiHandle;
    LcdHandle.RstPort    = GPIOC;
    LcdHandle.RstPin     = GPIO_PIN_7;
    LcdHandle.RsPort     = GPIOA;
    LcdHandle.RsPin      = GPIO_PIN_9;
    LcdHandle.CsPort     = GPIOA;
    LcdHandle.CsPin      = GPIO_PIN_8;

    HAL_GPIO_WritePin(hlcd->RstPort, hlcd->RstPin, RESET);
    HAL_Delay(2);
    HAL_GPIO_WritePin(hlcd->RstPort, hlcd->RstPin, SET);
    HAL_Delay(20);
    MOD_LCD_Command(hlcd,0x30u);
    HAL_Delay(2);
    MOD_LCD_Command(hlcd,0x30);
    MOD_LCD_Command(hlcd,0x30);
    MOD_LCD_Command(hlcd,0x39);
    MOD_LCD_Command(hlcd,0x14);
    MOD_LCD_Command(hlcd,0x56);
    MOD_LCD_Command(hlcd,0x6D);
    MOD_LCD_Command(hlcd,0x70);
    MOD_LCD_Command(hlcd,0x0C);
    MOD_LCD_Command(hlcd,0x06);
    MOD_LCD_Command(hlcd,0x01);
    HAL_Delay(10);
}
/**---------------------------------------------------------------
Brief.-  This function sends a command message through SPI to the LCD
Param.-  hlcd - This structure contains the elements to send
         the command to the LCD display
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_Command( LCD_HandleTypeDef *hlcd, uint8_t cmd )
{
    HAL_GPIO_WritePin(GPIOA,PIN_RS,RESET);
    HAL_GPIO_WritePin(GPIOA,PIN_CS,RESET);
    HAL_SPI_TransmitReceive(hlcd->SpiHandler,&cmd,&RxData, 1u, 5000u);
    HAL_GPIO_WritePin(GPIOA,PIN_CS,SET);    
    TM_DelayMicros(50u);
}
/**---------------------------------------------------------------
Brief.-  This function sends a data message through SPI to the LCD
Param.-  hlcd - This structure contains the elements to send
         the data to the LCD display
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_Data( LCD_HandleTypeDef *hlcd, uint8_t data )
{
    HAL_GPIO_WritePin(GPIOA,PIN_RS,SET);
    HAL_GPIO_WritePin(GPIOA,PIN_CS,RESET);    
    HAL_SPI_TransmitReceive(hlcd->SpiHandler,&data,&RxData, 1u, 5000u);
    HAL_GPIO_WritePin(GPIOA,PIN_CS,SET);
    TM_DelayMicros(50u);
}
/**---------------------------------------------------------------
Brief.-  This function sends a complete string through SPI to the LCD
Param.-  hlcd - This structure contains the elements to send
         the data to the LCD display
         size - the size of the string to send
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_String( LCD_HandleTypeDef *hlcd, char *str, uint8_t size)
{
    for(uint16_t i = 0;i < size; i++)
    {
        MOD_LCD_Data(hlcd,str[i]);
    }  
}
/**---------------------------------------------------------------
Brief.-  This function sets the cursors position in the display given   
         by the parameters
Param.-  hlcd - This structure contains the elements to set
         the cursor position in the LCD display
         row - Sets the row in the display 1 or 2 
         col - Sets the column in the display 0 to 15
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_SetCursor( LCD_HandleTypeDef *hlcd, uint8_t row, uint8_t col )
{
    MOD_LCD_Command(hlcd,(0x80u + row + (col * 0x40u)));
}

/**---------------------------------------------------------------
Brief.-  Function for delay for microseconds
Param.-  void
Return.- void
---------------------------------------------------------------*/
void TM_DelayMicros(uint32_t micros) {
    /* Multiply micros with multipler */
    /* Substract 10 */
    multiplier = HAL_RCC_GetHCLKFreq() / (HAL_RCC_GetHCLKFreq() / 4);
    micros = micros * multiplier;
    /* 4 cycles for one loop */
    while (micros--);
}
 /**---------------------------------------------------------------
Brief.-  Function for delay for milliseconds
Param.-  void
Return.- void
---------------------------------------------------------------*/
void TM_DelayMillis(uint32_t millis) 
{
    multiplier = HAL_RCC_GetHCLKFreq() / (HAL_RCC_GetHCLKFreq() / 4);
    /* Multiply millis with multipler */
    /* Substract 10 */
    millis = 1000 * millis * multiplier - 10;
    /* 4 cycles for one loop */
    while (millis--);
}
/**---------------------------------------------------------------
Brief.-  This function initializes the pins needed for the LCD 
         display
Param.-  hlcd - This structure contains the elements to configure
         the pins, and SPI handler for this LCD display
Return.- void
---------------------------------------------------------------*/
__weak void MOD_LCD_MspInit( LCD_HandleTypeDef *hlcd )
{
    (void)hlcd;
}