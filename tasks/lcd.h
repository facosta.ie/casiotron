/*lcd.h*/
#ifndef LCD_H__
#define LCD_H__


/**---------------------------------------------------------------
Brief.-  This function initializes, the SPI nneded for the control
         of the LCD display
Param.-  hlcd - This structure contains the elements to configure
         the pins, and SPI handler for this LCD display
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_Init( LCD_HandleTypeDef *hlcd );
/**---------------------------------------------------------------
Brief.-  This function initializes the pins needed for the LCD 
         display
Param.-  hlcd - This structure contains the elements to configure
         the pins, and SPI handler for this LCD display
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_MspInit( LCD_HandleTypeDef *hlcd );
/**---------------------------------------------------------------
Brief.-  This function sends a command message through SPI to the LCD
Param.-  hlcd - This structure contains the elements to send
         the command to the LCD display
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_Command( LCD_HandleTypeDef *hlcd, uint8_t cmd );
/**---------------------------------------------------------------
Brief.-  This function sends a data message through SPI to the LCD
Param.-  hlcd - This structure contains the elements to send
         the data to the LCD display
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_Data( LCD_HandleTypeDef *hlcd, uint8_t data );
/**---------------------------------------------------------------
Brief.-  This function sends a complete string through SPI to the LCD
Param.-  hlcd - This structure contains the elements to send
         the data to the LCD display
         size - the size of the string to send
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_String( LCD_HandleTypeDef *hlcd, char *str, uint8_t size );
/**---------------------------------------------------------------
Brief.-  This function sets the cursors position in the display given   
         by the parameters
Param.-  hlcd - This structure contains the elements to set
         the cursor position in the LCD display
         row - Sets the row in the display 1 or 2 
         col - Sets the column in the display 0 to 15
Return.- void
---------------------------------------------------------------*/
void MOD_LCD_SetCursor( LCD_HandleTypeDef *hlcd, uint8_t row, uint8_t col );
/**---------------------------------------------------------------
Brief.-  This function initializes the SPI for the LCD display
Param.-  void
Return.- void
---------------------------------------------------------------*/
void SPI_Init();
/**---------------------------------------------------------------
Brief.-  This function initializes the delay functions
Param.-  void
Return.- void
---------------------------------------------------------------*/
void TM_Delay_Init(void);
/**---------------------------------------------------------------
Brief.-  Function for delay for microseconds
Param.-  void
Return.- void
---------------------------------------------------------------*/
void TM_DelayMicros(uint32_t micros);
/**---------------------------------------------------------------
Brief.-  Function for delay for milliseconds
Param.-  void
Return.- void
---------------------------------------------------------------*/
void TM_DelayMillis(uint32_t millis);


#endif