/*queue.c*/
#include "globals.h"
#include "queue.h"

/**---------------------------------------------------------------
Brief.-  This function initializes the queue to start the reception
         of data, sets tail, head, full and empty flags. 
Param.-  hqueue this structure contains the data to be initialized
Return.- void
---------------------------------------------------------------*/
void HIL_QUEUE_Init( QUEUE_HandleTypeDef *hqueue )
{
    hqueue->Head  = 0u;
    hqueue->Tail  = 0u;
    hqueue->Empty = 1u;
    hqueue->Full  = 0u;
}

/**---------------------------------------------------------------
Brief.-  This function copies the information from data to the buffer
         configured in the initialization structure,the lenght to be 
         copied is initialized in the structure hqueue.Size
Param.-  hqueue - this function contains the data types to be copied and
         other variables for the operation of the queue 
         data - This pointer cotains the information to be copied
Return.- uint8 returns 1 if it could be written and 0 if not.
---------------------------------------------------------------*/
uint8_t HIL_QUEUE_Write( QUEUE_HandleTypeDef *hqueue, void *data )
{   
    uint8_t retval = FALSE;

	if(hqueue->Full == FALSE)
	{
        memcpy((hqueue->Buffer + (hqueue->Head * hqueue->Size)),data,hqueue->Size);
        hqueue->Head++;
        hqueue->Head %= hqueue->Elements;
                
        if(hqueue->Head == hqueue->Tail)
        {
            hqueue->Full = 1;
        }

        hqueue->Empty = FALSE;
	    retval = 1;
    }

	return retval;

}

/**---------------------------------------------------------------
Brief.-  This function copies the information from the buffer
         configured in the initialization structure to the pointer
         data, the lenght to be copied is initialized in the structure
         hqueue.Size
Param.-  hqueue - this function contains the data types to be copied and
         other variables for the operation of the queue 
         data - This pointer cotains the information to be copied
Return.- uint8 returns 1 if it could be read and 0 if not
---------------------------------------------------------------*/
uint8_t HIL_QUEUE_Read( QUEUE_HandleTypeDef *hqueue, void *data)
{
    uint8_t retval = FALSE;

	if(hqueue->Empty == FALSE)
	{
        memcpy(data,(hqueue->Buffer + (hqueue->Tail * hqueue->Size)),hqueue->Size);
        hqueue->Tail++;
        hqueue->Tail %= hqueue->Elements;
                
        if(hqueue->Tail == hqueue->Head)
        {
            hqueue->Empty = TRUE;
        }

        hqueue->Full = FALSE;
	    retval = TRUE;
    }

	return retval;
}

/**---------------------------------------------------------------
Brief.-  This function verifies if the queue contains elements to be
         read
Param.-  hqueue - this function contains the data types to be copied and
         other variables for the operation of the queue 
Return.- uint8 returns 1 if there are no more  elements to be read 
         and 0 if there are at least one element to be read
---------------------------------------------------------------*/
uint8_t HIL_QUEUE_IsEmpty( QUEUE_HandleTypeDef *hqueue )
{
    return hqueue->Empty;
}