/*queue.h*/

/**---------------------------------------------------------------
Brief.-  This function initializes the queue to start the reception
         of data, sets tail, head, full and empty flags. 
Param.-  hqueue this structure contains the data to be initialized
Return.- void
---------------------------------------------------------------*/
void HIL_QUEUE_Init( QUEUE_HandleTypeDef *hqueue );

/**---------------------------------------------------------------
Brief.-  This function copies the information from data to the buffer
         configured in the initialization structure,the lenght to be 
         copied is initialized in the structure hqueue.Size
Param.-  hqueue - this function contains the data types to be copied and
         other variables for the operation of the queue 
         data - This pointer cotains the information to be copied
Return.- uint8 returns 1 if it could be written and 0 if not.
---------------------------------------------------------------*/
uint8_t HIL_QUEUE_Write( QUEUE_HandleTypeDef *hqueue, void *data );

/**---------------------------------------------------------------
Brief.-  This function copies the information from the buffer
         configured in the initialization structure to the pointer
         data, the lenght to be copied is initialized in the structure
         hqueue.Size
Param.-  hqueue - this function contains the data types to be copied and
         other variables for the operation of the queue 
         data - This pointer cotains the information to be copied
Return.- uint8 returns 1 if it could be read and 0 if not
---------------------------------------------------------------*/
uint8_t HIL_QUEUE_Read( QUEUE_HandleTypeDef *hqueue, void *data );

/**---------------------------------------------------------------
Brief.-  This function verifies if the queue contains elements to be
         read
Param.-  hqueue - this function contains the data types to be copied and
         other variables for the operation of the queue 
Return.- uint8 returns 1 if there are no more  elements to be read 
         and 0 if there are at least one element to be read
---------------------------------------------------------------*/
uint8_t HIL_QUEUE_IsEmpty( QUEUE_HandleTypeDef *hqueue );