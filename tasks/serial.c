/*Serial.c*/
#include "globals.h"
#include "serial.h"

uint8_t RxByte = 0u; 
uint8_t RxBuffer[100u] = {0u};             /*Buffer donde se respalda la informacion recibida*/
uint8_t CMD_ERROR[] = "ERROR\n\r";
uint8_t CMD_OK[] = "OK\n\r";
Usart_states Usart_State = CLEAR_CMD;

/**---------------------------------------------------------------
Brief.-  This function is called everytime there's an interrupt
         in a serial.
Param.-  void
Return.- void
---------------------------------------------------------------*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if(huart->Instance == USART2)
  {
    if((RxByte == '\r') || (isprint(RxByte) != 0u))
    {
      HIL_QUEUE_Write(&SerialQueue,&RxByte);
    }
    /* Receive one byte in interrupt mode */ 
    HAL_UART_Receive_IT(&Handler_Uart, &RxByte, 1u);
  }
}

/**---------------------------------------------------------------
Brief.-  This function initializes the peripheral UART2 and its 
         interrupt handler.
Param.-  void
Return.- void
---------------------------------------------------------------*/
void Serial_Init(void)
{
  Handler_Uart.Instance = USART2;
  Handler_Uart.Init.BaudRate = 9600;
  Handler_Uart.Init.WordLength = UART_WORDLENGTH_8B;
  Handler_Uart.Init.StopBits = UART_STOPBITS_1;
  Handler_Uart.Init.Parity = UART_PARITY_NONE;
  Handler_Uart.Init.Mode = UART_MODE_TX_RX;
  Handler_Uart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  Handler_Uart.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  HAL_UART_Init(&Handler_Uart);

  HAL_NVIC_SetPriority(USART2_IRQn, 0u, 0u);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
  HAL_UART_Receive_IT(&Handler_Uart, &RxByte, 1u);   /* Peripheral interrupt init*/
}

/**---------------------------------------------------------------
Brief.-  This function executes the functions related to serial
         tasks
Param.-  void
Return.- void
---------------------------------------------------------------*/
void Serial_Task(void)
{
  Serial_IDLE();
  Serial_MSG();
}

/**---------------------------------------------------------------
Brief.-  This function reads the values received in the serial queue
         while there are elements available to be read or a complete
         message is received.
Param.-  void
Return.- void
---------------------------------------------------------------*/
void Serial_IDLE(void)
{
  static uint16_t index = 0u;

  while(HIL_QUEUE_IsEmpty(&SerialQueue) == 0u)
  {
    HAL_NVIC_DisableIRQ(USART2_IRQn);
    HIL_QUEUE_Read(&SerialQueue,&RxBuff);
    HAL_NVIC_EnableIRQ(USART2_IRQn);

    if(RxBuff == '\r')
    {
      Usart_State = NEW_CMD;
      RxBuffer[index] = '\0';
      index = 0u;
      break;
    }
    else
    {
      RxBuffer[index] = RxBuff;
      index++;  
    }
  }
}

uint8_t StrToInt(char* string)
{
  uint8_t DigitOK = 0;
  uint8_t retval = 0xFF;
  uint8_t i = 0;
  uint8_t DigitCtr = 0;

  if(string != NULL)
  {
    while(string[i] != '\0')
    {
      if(isdigit(string[i]) == 1u)
      {
        DigitCtr++;
      }
      i++;
    }

    if(strlen(string) == DigitCtr)
    {
      retval = atoi(string);
    }
  }
//poner los pines como open drain o colector abierto
//para el I2C 
  return retval;
}

/**---------------------------------------------------------------
Brief.- This function gets the serial command received,
Param.- uint8_t *CMD_Buffer this variable points to the serial buffer
        received to be analized
Return.- Return the type o message received
---------------------------------------------------------------*/
void Serial_MSG(void)
{
  char* auxData;
  char  rtcData[3];
  char  token[2] = ",";
  char  token2[2] = "=";
  uint16_t auxYear = 0u;
  uint8_t case_CMD = 0xFFu;

  if(Usart_State == NEW_CMD)
  {
    Usart_State = CLEAR_CMD;

    case_CMD = Serial_Get_CMD(&RxBuffer);

    switch (case_CMD)
    {
      case TIME_CMD:
      {
        auxData =    strtok((char*)RxBuffer,token2);
        auxData = strtok(NULL,token);
        rtcData[0] = StrToInt(auxData);
        auxData = strtok(NULL,token);
        rtcData[1] = StrToInt(auxData);
        auxData = strtok(NULL,token);
        rtcData[2] = StrToInt(auxData);

        if(ValidateTime(rtcData[0],rtcData[1],rtcData[2]) == TRUE)
        {
          RTC_Data_Config.msg = TIME;
          RTC_Data_Config.param1 = rtcData[0];
          RTC_Data_Config.param2 = rtcData[1];
          RTC_Data_Config.param3 = rtcData[2];
          HIL_QUEUE_Write(&MsgQueue,&RTC_Data_Config);
          HAL_UART_Transmit_IT(&Handler_Uart, CMD_OK, sizeof(CMD_OK) - 1u);
        }
        else
        {
          HAL_UART_Transmit_IT(&Handler_Uart, CMD_ERROR, sizeof(CMD_ERROR) - 1u);
        }
        break;
      }
      case DATE_CMD:
      {
        auxData =  strtok((char*)RxBuffer,token2);
        rtcData[0] = RTC_ByteToBcd2(atoi(strtok(0u,token)));
        rtcData[1] = RTC_ByteToBcd2(atoi(strtok(0u,token)));
        rtcData[2] = RTC_ByteToBcd2(atoi(strtok(0u,token)) - 2000u);

        if((rtcData[0] < DAY_31) && (rtcData[1] < MONTH_12) && (rtcData[2] < Year_99))
        {
          RTC_Data_Config.msg = DATE;
          RTC_Data_Config.param1 = rtcData[0];
          RTC_Data_Config.param2 = rtcData[1];
          RTC_Data_Config.param3 = rtcData[2];
          HIL_QUEUE_Write(&MsgQueue,&RTC_Data_Config);
          HAL_UART_Transmit_IT(&Handler_Uart, CMD_OK, sizeof(CMD_OK) - 1u);
        }
        else 
        {
          HAL_UART_Transmit_IT(&Handler_Uart, CMD_ERROR, sizeof(CMD_ERROR) - 1u);
        }
        break;
      }
      case ALARM_CMD:
      {
          auxData =    strtok((char*)RxBuffer,token2);
          rtcData[0] = RTC_ByteToBcd2(atoi(strtok(0u,token)));
          rtcData[1] = RTC_ByteToBcd2(atoi(strtok(0u,token)));
          rtcData[2] = RTC_ByteToBcd2(atoi(strtok(0u,token)));
          
          if((rtcData[0] < HOUR_24) && (rtcData[1] < MINUTE_60) && (rtcData[2] < SECOND_60))
          {
            RTC_Data_Config.msg = ALARM;
            RTC_Data_Config.param1 = rtcData[0];
            RTC_Data_Config.param2 = rtcData[1];
            RTC_Data_Config.param3 = rtcData[2];
            HIL_QUEUE_Write(&MsgQueue,&RTC_Data_Config);
            HAL_UART_Transmit_IT(&Handler_Uart, CMD_OK, sizeof(CMD_OK) - 1u);
            Flag_AlarmSet = 1u;
          }
          else 
          {
            HAL_UART_Transmit_IT(&Handler_Uart, CMD_ERROR, sizeof(CMD_ERROR) - 1u);
          }
          break;
        }
        default:
        HAL_UART_Transmit_IT(&Handler_Uart, CMD_ERROR, sizeof(CMD_ERROR) - 1u);
        break;
      }
    }
}

/**---------------------------------------------------------------
Brief.- This function analizes the serial buffer gotten and 
        determines the kind of message received
Param.- uint8_t *CMD_Buffer this variable points to the serial buffer
        received to be analized
Return.- Return the type o message received
---------------------------------------------------------------*/
uint8_t Serial_Get_CMD(uint8_t *CMD_Buffer)
{
  uint8_t retval;

  if(memcmp("AT+TIME=", RxBuffer, sizeof("AT+TIME=")-1u) == 0u)
  {
    retval = (uint8_t)TIME_CMD;
  }
  else if(memcmp("AT+DATE=", RxBuffer, sizeof("AT+DATE=")-1u) == 0u)
  {
    retval = (uint8_t)DATE_CMD;
  }
  else if(memcmp("AT+ALARM=", RxBuffer, sizeof("AT+ALARM=") - 1u) == 0u)
  {
    retval = (uint8_t)ALARM_CMD;
  }
  else  
  {
    retval = (uint8_t)CMD_ERROR;
  }

  return retval;
}


uint8_t ValidateTime(uint8_t Hour,uint8_t Minute,uint8_t Second)
{
  uint8_t retval = FALSE;

  if((Hour < HOUR_24) && (Minute < MINUTE_60) && (Second < SECOND_60))
  {
    retval = TRUE;
  }

return retval;
}