/*Serial.h*/
#ifndef SERIAL_H__
#define SERIAL_H__

#define HOUR_24   0x24u
#define MINUTE_60 0x60u
#define SECOND_60 0x60u

#define DAY_31    0x32u
#define MONTH_12  0x13u
#define Year_99   0x99u

#define TIME_CMD  0u
#define DATE_CMD  1u
#define ALARM_CMD 2u

/**---------------------------------------------------------------
Brief.-  This function initializes the peripheral UART2 and its 
         interrupt handler.
Param.-  void
Return.- void
---------------------------------------------------------------*/
void Serial_Init(void);

/**---------------------------------------------------------------
Brief.-  This function executes the functions related to serial
         tasks
Param.-  void
Return.- void
---------------------------------------------------------------*/
void Serial_Task(void);

/**---------------------------------------------------------------
Brief.-  This function reads the values received in the serial queue
         while there are elements available to be read or a complete
         message is received.
Param.-  void
Return.- void
---------------------------------------------------------------*/
void Serial_IDLE(void);

/**---------------------------------------------------------------
Brief.- This function gets the serial command received,
Param.- uint8_t *CMD_Buffer this variable points to the serial buffer
        received to be analized
Return.- Return the type o message received
---------------------------------------------------------------*/
void Serial_MSG(void);

/**---------------------------------------------------------------
Brief.- This function analizes the serial buffer gotten and 
        determines the kind of message received
Param.- uint8_t *CMD_Buffer this variable points to the serial buffer
        received to be analized
Return.- Return the type o message received
---------------------------------------------------------------*/
uint8_t Serial_Get_CMD(uint8_t *CMD_Buffer);

uint8_t ValidateTime(uint8_t Hour,uint8_t Minute,uint8_t Second);

#endif