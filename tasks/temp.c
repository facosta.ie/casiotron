

void MOD_TEMP_Init( TEMP_HandleTypeDef *htemp );
/*Inicializa el sensor para que este esté listo para recibir datos a desplegar en su pantalla, 
mediante la estructura TEMP_HandleTypeDef se debe seleccionar el handler del I2C (ya configurado) 
que se va a usar así como el pin que leerá si ocurre una alarma . 
NOTA: esta función no debe inicializar el I2C ni el puerto GPIO que usará el driver.*/

void MOD_TEMP_MspInit( TEMP_HandleTypeDef *htemp );
/*Función de extra inicialización para agregar código exclusivo dentro de la aplicación.
Esta función se debe mandar llamar dentro de la función MOD_TEMP_Init y deberá definirse como weak para 
que se pueda redefinir dentro de la aplicación, esta función es un buen lugar para inicializar el pin que 
leerá la señal de alarma*/
	
uint16_t MOD_TEMP_Read( TEMP_HandleTypeDef *htemp );
/*Pregunta al sensor de temperatura la ultima lectura que este ya tiene disponible.*/

void MOD_TEMP_SetAlarms( TEMP_HandleTypeDef *htemp, uint16_t lower, uint16_t upper  );
/*Establece el valor de temperatura para la alarma inferior y la alarma superior, cuando el sensor supere o 
baje de esos valores el pin de alarma deberá activarse */

void MOD_TEMP_DisableAlarm( LCD_HandleTypeDef *hlcd );
/*Deshabilita la ventana de alarma establecida.*/
